module recreate-appreg

go 1.15

require (
	gitlab.cern.ch/paas-tools/operators/authz-operator v0.0.0-20210128170531-8308ec545e0f
	k8s.io/apimachinery v0.18.6
	k8s.io/client-go v0.18.6
)