# Recreate AppReg

git clone this-repo

helm upgrade --install recreate-appreg ./chart -n default

oc create job --from=cronjob.batch/recreate-appreg recreation -n default

oc delete job recreation -n default

helm uninstall recreate-appreg -n default