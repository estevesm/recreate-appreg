FROM golang:1.15.0

# Copy the code from the host and compile it
WORKDIR $GOPATH/src/gitlab.cern.ch/paas-tools/scripts/recreate-appreg
COPY . ./
RUN CGO_ENABLED=1 GOOS=linux go build -a -installsuffix nocgo -mod vendor -o /main .

ENTRYPOINT ["/main"]
