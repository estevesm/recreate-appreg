/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Note: the example only works with the code within the same release/branch.
package main

import (
	"context"
	"flag"
	"log"
	"path/filepath"
	"strings"

	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	//
	// Uncomment to load all auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth"
	//
	// Or uncomment to load specific auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth/azure"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/openstack"
)

func main() {
	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	var err error
	var config *rest.Config

	if *kubeconfig == "" {
		log.Printf("Using in-cluster configuration")
		config, err = rest.InClusterConfig()
	} else {
		config, err = clientcmd.BuildConfigFromFlags("", *kubeconfig)
	}
	if err != nil {
		panic(err.Error())
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	schemeClient := newClientForConfig(*kubeconfig)

	namespaces, err := clientset.CoreV1().Namespaces().List(context.TODO(), metav1.ListOptions{})
	for _, namespace := range namespaces.Items {
		if !strings.HasPrefix(namespace.Name, "openshift-") && namespace.Name != "openshift" && !strings.HasPrefix(namespace.Name, "kube") && namespace.Name != "default" && namespace.Name != "webeos" {
			appReg := &webservicesv1alpha1.ApplicationRegistration{}
			err := schemeClient.Get().
				Resource("applicationregistrations").
				Namespace(namespace.Name).
				Name(namespace.Name).
				VersionedParams(&metav1.ListOptions{}, scheme.ParameterCodec).
				Do(context.TODO()).Into(appReg)

			if errors.IsNotFound(err) {
				newAppReg := getApplicationRegistration(namespace.Name, namespace.Annotations["openshift.io/description"], namespace.Annotations["openshift.io/requester"])
				result := schemeClient.Post().
					Namespace(namespace.Name).
					Resource("applicationregistrations").
					Body(newAppReg).
					Do(context.Background())
				if result.Error() != nil {
					println(result.Error().Error())
				} else {
					println("AppReg created with success for project ", namespace.Name)
				}
			}
		}
	}
}
func getApplicationRegistration(projectName string, projectDescription string, projectAdminUser string) *webservicesv1alpha1.ApplicationRegistration {

	// 	# register the project in the Application Portal
	// - apiVersion: webservices.cern.ch/v1alpha1
	//   kind: ApplicationRegistration
	//   metadata:
	//     name: ${PROJECT_NAME}
	//     namespace: ${PROJECT_NAME}
	//   spec:
	//     applicationName: "${PROJECT_NAME}"     # unique
	//     description: "${PROJECT_DESCRIPTION}"
	//     initialOwner:
	//       username: "${PROJECT_ADMIN_USER}"
	//     initialRoles:
	//     - name: default-role
	//       required: true
	//       applyToAllUsers: true
	//       description: "Users must be from CERN or eduGAIN to have access"
	//       displayName:  "Default Allowed Users"
	//       minimumLevelOfAssurance: 4

	return &webservicesv1alpha1.ApplicationRegistration{
		ObjectMeta: metav1.ObjectMeta{
			Name:      projectName,
			Namespace: projectName,
			Annotations: map[string]string{
				"recreated": "This resource was recreated during for https://cern.service-now.com/service-portal?id=outage&n=OTG0061888 see https://codimd.web.cern.ch/LMNdXxdoSG6FuYoAdldOZQ",
			},
		},
		Spec: webservicesv1alpha1.ApplicationRegistrationSpec{
			ApplicationName: projectName,
			Description:     projectDescription,
			InitialOwner: webservicesv1alpha1.InitialOwner{
				Username: projectAdminUser,
			},
		},
	}
}
